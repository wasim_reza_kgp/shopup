import collections

book_data = collections.namedtuple('book_data', ['name', 'image_link', 'rating', 'price', 'in_stock', 'category'])
category_data = collections.namedtuple('category_data', ['name', 'link'])