import os

import requests
from bs4 import BeautifulSoup


def get_parsed_html_from_url(url):
    result = requests.get(url)
    soup = BeautifulSoup(result.text, 'html.parser')
    return soup

def create_dir_if_not_exist(output_dir):
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
