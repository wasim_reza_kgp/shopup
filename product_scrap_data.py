import itertools

from scrap_util import get_parsed_html_from_url
from scrap_data_models import book_data
import multiprocessing
import requests
import re
import pandas as pd


def get_books_url_from_page(page_url):
    soup = get_parsed_html_from_url(page_url)
    return ["/".join(page_url.split("/")[:-1]) + "/" + x.div.a.get('href') for x in
            soup.findAll("article", class_="product_pod")]


def get_all_page_urls():
    pages_urls = list()
    new_page = "http://books.toscrape.com/catalogue/page-1.html"
    while requests.get(new_page).status_code == 200:
        pages_urls.append(new_page)
        new_page = pages_urls[-1].split("-")[0] + "-" + str(
            int(pages_urls[-1].split("-")[1].split(".")[0]) + 1) + ".html"
    return pages_urls


def get_all_book_urls(pages_urls):
    booksURLs = list()
    for page in pages_urls:
        booksURLs.extend(get_books_url_from_page(page))
    return booksURLs


def get_book_scrap_data(book_url):
    soup = get_parsed_html_from_url(book_url)
    name = soup.find("div", class_=re.compile("product_main")).h1.text
    image_link = book_url.replace("index.html", "") + soup.find("img").get("src")
    rating = soup.find("p", class_=re.compile("star-rating")).get("class")[1]
    price = soup.find("p", class_="price_color").text[2:]
    in_stock = re.sub("[^0-9]", "", soup.find("p", class_="instock availability").text)
    category = soup.find("a", href=re.compile("../category/books/")).get("href").split("/")[3]
    return book_data(name, image_link, rating, price, in_stock, category)


def get_book_data_df():
    cpu_count = multiprocessing.cpu_count()
    page_urls = get_all_page_urls()
    with multiprocessing.Pool(cpu_count) as pool:
        book_urls_list = pool.map(get_books_url_from_page, page_urls)
    book_urls = list(itertools.chain(*book_urls_list))

    with multiprocessing.Pool(cpu_count * 2) as pool:
        book_data_list = pool.map(get_book_scrap_data, book_urls)
    df = pd.DataFrame.from_records(book_data_list,
                                   columns=['Name', 'Image Link', 'Rating', 'Price', 'In-Stock', 'Category'])
    return df