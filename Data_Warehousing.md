# Data warehousing of http://books.toscrape.com/
#Storage of data
1. We are storing the data in parquet format
2. We can store the data in aws s3 and from their we can process using
    1. Spark(Preferred)
    2. Hive
    3. Drill
    4. Presto
3. We can store the data in cassandra also
    1. Partition Key => (book_name)
    2. No requirement of clustering key
    3. We can query over cassandra using spark with the help of spark-cassandra connector
    