import unittest

import product_scrap_data
import category_scrap
from scrap_data_models import book_data, category_data


class BookScrapTest(unittest.TestCase):
    def test_valid_book_url(self):
        test_page = 'http://books.toscrape.com/catalogue/page-1.html'
        test_book_url = 'http://books.toscrape.com/catalogue/soumission_998/index.html'
        book_url_list = product_scrap_data.get_books_url_from_page(test_page)
        self.assertEqual(len(book_url_list), 20)
        self.assertTrue(test_book_url in book_url_list)

    def test_valid_parsing(self):
        test_book_url = 'http://books.toscrape.com/catalogue/soumission_998/index.html'
        expected_book = book_data('Soumission','http://books.toscrape.com/catalogue/soumission_998/../../media/cache/ee'
                                           +'/cf/eecfe998905e455df12064dba399c075.jpg',
                  'One', '50.10', '20', 'fiction_10')
        self.assertEqual(expected_book, product_scrap_data.get_book_scrap_data(test_book_url))

class CategoryScrapTest(unittest.TestCase):
    def test_valid_category_data(self):
        test_cat = category_data('Travel','http://books.toscrape.com/catalogue/category/books/travel_2/index.html')
        self.assertTrue(test_cat in category_scrap.get_category_data())

if __name__ == '__main__':
    unittest.main()

