# Scrapping data in multiprocessing 

Scrapping and storing data of http://books.toscrape.com/
The scrapping module written in multiprocessing manner to reduce the latency

## Prereuisite
python3, pip3

For ubuntu please follow
https://docs.python-guide.org/starting/install3/linux/

For mac-os-x  
brew install python3
(Assuming brew is the default package manager)
# virtual env creation(Optional)
1. python3 -m venv /path/to/new/virtual/environment
2. cd /path/to/new/virtual/environment
3. source bin/activate

### For virtualenv use python instead of python3

# build and test Steps
1. git clone git@bitbucket.org:wasim_reza_kgp/shopup.git
2. cd shopup
3. pip3 install -r requirements.txt
4. For osx run 
 **export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES**
5. To run the Main Application:  
    **python3 main.py**  
5. To Run The Test Cases :  
**python3 -m unittest test_basic**
6. To run the output for warehousing
  **python3 main_warehouse.py**
  output will be in gzip format in output directory

## All outputs are default to output directory


# Future scope of this project
1. Dockerization
2. Supporting multiple python version
3. Adding Click module for supporting command line utility for scrapping the datag
4. Setup module creation and local installation
5. Adding test cases for file reading and writing
6. Defining environment variables for testing and development
7. For huge volumetric data we need to implement pyspark(as python module) or dask for multinode deployment.


