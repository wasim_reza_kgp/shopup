from category_scrap import get_category_df
from product_scrap_data import get_book_data_df
from scrap_util import create_dir_if_not_exist
import contants

if __name__ == '__main__':
    create_dir_if_not_exist(contants.output_dir)
    book_df = get_book_data_df()
    book_df.to_parquet(contants.book_data_path_gzip, compression='gzip')
    del book_df
    category_df = get_category_df()
    category_df.to_parquet(contants.category_data_path_gzip, compression='gzip')
    del category_df
