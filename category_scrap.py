import multiprocessing

from scrap_util import get_parsed_html_from_url
from scrap_data_models import category_data
import re
import pandas as pd

main_url = "http://books.toscrape.com/"


def get_category_data():
    category_data_list = list()
    soup = get_parsed_html_from_url(main_url)
    for x in soup.find_all("a", href=re.compile("catalogue/category/books")):
        category_data_list.append(category_data(x.contents[0].strip(), main_url + x.get('href')))
    return category_data_list[1:]


def get_category_df():
    category_data_list = get_category_data()
    df = pd.DataFrame.from_records(category_data_list,
                                   columns=['Name', 'Link'])
    return df



